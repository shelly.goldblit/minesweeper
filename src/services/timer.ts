let timeOut: NodeJS.Timeout | undefined;

export const stopTimer = () => {
  if (timeOut) {
    clearInterval(timeOut);
    timeOut = undefined;
  }
};
export const startTimer = (callback: (...args: any[]) => void, ...args: any[]) => {
  if (!timeOut) {
    timeOut = setInterval(callback, 1000, args);
  }
};


