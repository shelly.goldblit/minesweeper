import '@testing-library/jest-dom/extend-expect';
import { Chance } from 'chance';
import { getNumberOfFlagged, getNumberOfMines, getNumberOfRevealed, isGameLost, isGameStarted, isGameWon } from '../selectors/game-selectors';
import { markCell } from './cell-marker';
import { revealeCell } from './cells-revealer';
import { getNewGame, startGame } from './game-generator';

describe('services tests', () => {
  const chance = new Chance();
  

  describe ('new game', () => {
    const size = chance.integer({ min: 2, max: 20 });
    const minesField = getNewGame(size);

    test('game started should be false before first click', () => {
      expect(isGameStarted(minesField)).toBe(false);
    });
  })

  describe ('started game', () => {
    const size = chance.integer({ min: 2, max: 3 });
    const mines = chance.integer({ min: 1, max: size*size - 1 });
    const firstCellKey = chance.integer({ min: 0, max: size*size - 1 });
    const startedGame = startGame({firstCellKey, size, mines});
    const minesField = revealeCell({minesField: startedGame, cellKey: firstCellKey, size, mines});

    test('should calculate number of mines correctly', () => {
      expect(getNumberOfMines(minesField)).toBe(mines);
    });
  
    test('game started should be true after first click', () => {
      expect(isGameStarted(minesField)).toBe(true);
    });
    
    test('first cell clicked should never be a mine', () => {
      expect(isGameLost(minesField)).toBe(false);
    });

    test('first cell should be revealed', () => {
      expect(minesField[firstCellKey].isRevealed).toBe(true);
    });

    test('should be at least one cell revealed', () => {
      expect(getNumberOfRevealed(minesField)).toBeGreaterThan(0);
    });
  });

  describe('game actions', () => {
    const size= 3;
    const mines= chance.integer({ min: 2, max: 7 });
    const maxMines= 8;
    const middleCell= 4;
    const topLeftCornerCell = 0;
    const firstCellKey= middleCell;

    test('neighbor mines should be calculated correctly', () => {
      const minesField= revealeCell({minesField: getNewGame(size), cellKey: firstCellKey, size, mines});
      expect(minesField[firstCellKey].neighbours).toBe(mines);
    });
  
    describe('cell markings', () =>
    {
      const minesField= revealeCell({minesField: getNewGame(size), cellKey: firstCellKey, size, mines});
      const markedCell= topLeftCornerCell;

      test('should mark cell', () => {
        expect(markCell(minesField, topLeftCornerCell)[topLeftCornerCell].isMarkedAsMine).toBe(true);
      });
  
      test('should update count down of marked mines', () => {
        expect(getNumberOfFlagged(markCell(minesField, topLeftCornerCell))).toBe(1);
      });
  
      test('should update count down of marked mines when marking cell as suspect', () => {
        const minesFieldWithFlaggedCell= markCell(minesField, markedCell)
        expect(getNumberOfFlagged(markCell(minesFieldWithFlaggedCell, markedCell))).toBe(0);
      });
  
      test('should change marked as mine cell to marked as suspect', () => {
        const minesFieldWithFlaggedCell = markCell(minesField, markedCell)
        expect(markCell(minesFieldWithFlaggedCell, markedCell)[markedCell]?.isMarkedAsSuspect).toBe(true);
      });
  
      test('should change marked as mine cell when marked as suspect', () => {        
        const minesFieldWithFlaggedCell = markCell(minesField, markedCell)
        expect(markCell(minesFieldWithFlaggedCell, markedCell)[markedCell]?.isMarkedAsMine).toBe(false);
      });
  
    });

    // TODO
    test.skip('game lost status should be true when a mine is revealed', () => {
      const cellToReveale = topLeftCornerCell;
      const minesField = revealeCell({minesField: getNewGame(size), size, mines: maxMines-1, cellKey: middleCell});
      expect(isGameLost(revealeCell({minesField, size, mines: maxMines, cellKey: cellToReveale}))).toBe(true);
    });

    test('game won status should be true when all none mine cells are revealed', () => {
      const cellToReveale = topLeftCornerCell;
      const minesField = getNewGame(size);
      expect(isGameWon(revealeCell({minesField, size, mines: maxMines, cellKey: cellToReveale}))).toBe(true);
    });
  });

  
  // test('should reaveale only cells with no mines around', () => {
  //   const key = coord2Key(1, 1);
  //   // const bombs = chance.integer({ min: 0, max: 8 });
  //   let board = getNewGeneratetGame(3, 1);
  //   // const boardSize = randomSize*randomSize;
  //   // debugger;
  //   board = getGameWithRevealedCell(board, key);
  //   const actual = getNumberOfRevealed(board);
  //   expect(actual).toBe(1);
  // });

  // test('should reaveale all cells with no mines around', () => {

  //   let board = getNewGeneratetGame(randomSize,0);
  //   // const boardSize = randomSize*randomSize;
  //   // debugger;
  //   board = getGameWithRevealedCell(board, randomKey);
  //   expect(getNumberOfRevealed(board)).toBe(randomSize*randomSize);
  //   expect(isGameWon(board)) .toBe(true);
  // });
  // test('should calculate number of revealed correctly', () => {
  //   const actual = calcNumberOfRevealed([
  //     [CellState.Initial, CellState.MarkedAsBomb, CellState.MarkedAsQM],
  //     [CellState.Initial, CellState.Revealed, CellState.MarkedAsQM],
  //     [CellState.Initial, CellState.MarkedAsQM, CellState.Revealed],
  //   ]);
  //   expect(actual).toBe(2);
  // });

 
});
