import { isGameLost, isGameStarted, isGameWon } from '../selectors/game-selectors';
import { CellData } from '../types';


const shouldMarkCell = (minesField: CellData[], cellKey: number): boolean =>
!minesField[cellKey].isRevealed &&
  isGameStarted(minesField) &&
  !isGameLost(minesField) &&
  !isGameWon(minesField);

const markSingleCell = (cellData: CellData): CellData => {
  return {
    ...cellData,
    isMarkedAsSuspect: cellData.isMarkedAsMine,
    isMarkedAsMine: !cellData.isMarkedAsMine && !cellData.isMarkedAsSuspect,
  };
};

export const markCell = (
  minesField: CellData[],
  cellKey: number,
): CellData[] =>
  {
    if (!shouldMarkCell(minesField, cellKey)) {
      return minesField;
    }
    const newMinesField = minesField.slice();
    newMinesField[cellKey] = markSingleCell(minesField[cellKey]);

    return newMinesField;
  }
