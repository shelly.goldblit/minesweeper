import { getNeighbourCellsKeys, shuffleArray } from './array-utils';
import { CellData } from '../types';

const countNeghibours = ({minesField, key, size}: {minesField: CellData[], key: number, size: number}) : number=> {
  const neighbourCells = getNeighbourCellsKeys(key, size);
  return minesField.reduce((neighbours, cell, key) => 
    neighbourCells.includes(key) && cell.isMine?  neighbours+1 : neighbours, 0 );
}

const getNeighboursMap = (minesField: CellData[], size: number) => 
  minesField.map((cell, key)=> ({...cell, neighbours: countNeghibours({minesField, key, size})}));

const insertFirstCell = (minesField: CellData[],firstCellKey: number) => {
  const newMinesField = minesField.slice();
  newMinesField.splice(firstCellKey, 0, { isMine: false, neighbours : 0});
  return newMinesField;
}
export const getNewGame = (size: number): CellData[] => Array(size*size).fill({neighbours: 0});

export const startGame = ({firstCellKey, size, mines}: {firstCellKey: number, size: number, mines: number}) => {
  const unshuffeledMinesField: CellData[] = Array(size*size-1).fill({}).map((_cell, i)=> ({neighbours: 0, isMine: i < mines}));
  const shuffeledMinesField = shuffleArray(unshuffeledMinesField);
  const minesField = insertFirstCell(shuffeledMinesField, firstCellKey);

  return getNeighboursMap(minesField, size);
};

export const terminateGame = (minesFeild: CellData[]) => 
  minesFeild.map(cell=> ({...cell, isRevealed: cell.isRevealed || (cell.isMine && !cell.isMarkedAsMine)}));
