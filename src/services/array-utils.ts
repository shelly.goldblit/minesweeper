export const shuffleArray= <T> (array: T[]) => {
  const result = [...array];
  for (let i = result.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [result[i], result[j]] = [result[j], result[i]];
  }
  return result;
}

// TODO: needs improvement
export const getNeighbourCellsKeys= (key : number,size : number)=> {
  const row = key/size;
  const col = key%size;

  const isRowAboveInField = row-1 >= 0;
  const isColToTheLeftInField = col-1 >= 0;
  const isRowBelowInField = row+1 < size;
  const isColToTheRightInField = col+1 < size;

  const toTheLeft= (key: number) => key-1;
  const toTheRight= (key: number) => key+1;

  const above= (key: number) => key-size;
  const below= (key: number) => key+size;

  const neighbourCellsKeys = [
    isRowAboveInField && isColToTheLeftInField? above(toTheLeft(key)) : -1,
    isRowAboveInField? above(key) : -1,
    isRowAboveInField && isColToTheRightInField? above(toTheRight(key)): -1,
    isColToTheLeftInField? toTheLeft(key): -1,
    isColToTheRightInField? toTheRight(key) : -1,
    isRowBelowInField && isColToTheLeftInField? below(toTheLeft(key)): -1,
    isRowBelowInField? below(key): -1,
    isRowBelowInField && isColToTheRightInField? below(toTheRight(key)): -1
  ];

  return neighbourCellsKeys.filter(key => key !== -1);
}