import { GameData } from "../types";

export const saveGame = (id: string, data: GameData) => localStorage.setItem(id, JSON.stringify(data));
    
export const loadGame = (id: string) => JSON.parse(localStorage.getItem(id) || '{}');