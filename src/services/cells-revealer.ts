import {
  isGameStarted,
  isGameLost,
  isGameWon,
  getNumberOfFlagged,
} from '../selectors/game-selectors';
import { startGame, terminateGame } from './game-generator';

import {
  CellData
} from '../types';
import { getNeighbourCellsKeys } from './array-utils';

const shouldRevealeAdjecentCells = (cellData: CellData) =>
  cellData.neighbours === 0 && !cellData.isMine;

const shouldRevealeSingleCell = (cellData: CellData): boolean =>
  cellData &&
  !cellData.isRevealed &&
  !cellData.isMarkedAsSuspect &&
  !cellData.isMarkedAsMine;

const shouldRevealeCell = (minesField: CellData[], cellKey: number): boolean =>
  !isGameLost(minesField) &&
  !isGameWon(minesField) &&
  shouldRevealeSingleCell(minesField[cellKey]);

const revealeSingleCell = (minesField: CellData[], cellKey: number) => {
  const newMinesField = minesField.slice();
  newMinesField[cellKey] =  { ...minesField[cellKey], isRevealed: true, didExplode: minesField[cellKey].isMine };
  return newMinesField;
};

export const revealeCellFirstOrderNeighbours = ({ minesField, cellKey, size, } : {
  minesField: CellData[],
  cellKey: number,
  size: number,
  })  : CellData[] => {

  const neighbourKeys = getNeighbourCellsKeys(cellKey, size);
  const flaggedNeighbors = getNumberOfFlagged(minesField.filter((_cell, key)=> neighbourKeys.includes(key)))
  if (!minesField[cellKey].isRevealed || minesField[cellKey].neighbours !== flaggedNeighbors)
  {
    return minesField;
  }
  return minesField.map((cell,key)=> neighbourKeys.includes(key) && !cell.isMine ? {...cell, isRevealed: true} : cell);
}

export const revealeCell = (
  { minesField, cellKey, size, mines, } : {
    minesField: CellData[],
    cellKey: number,
    size: number,
    mines: number,}
): CellData[] => {
  if (!shouldRevealeCell(minesField, cellKey)) {
    return minesField;
  }

  let newMinesField = isGameStarted(minesField)
  ? minesField
  : startGame({firstCellKey: cellKey, size, mines});
  newMinesField = revealeCellWithAdjecentCells({minesField: newMinesField, cellKey, size});
  if (isGameLost(newMinesField)) {
     newMinesField = terminateGame(newMinesField);
  }
  return newMinesField;
};

const revealeCellWithAdjecentCells = (
  {minesField, cellKey, size} : {
    minesField: CellData[],
    cellKey: number,
    size: number
  }
): CellData[] => {

  if (!shouldRevealeCell(minesField, cellKey)) {
    return minesField;
  }

  const neighbourCells = getNeighbourCellsKeys(cellKey, size);
  let newMinesField = revealeSingleCell(minesField, cellKey);

  if (shouldRevealeAdjecentCells(newMinesField[cellKey])) {
    neighbourCells.filter(cellKey => cellKey >= 0 && cellKey < minesField.length).forEach(cellKey => {
      newMinesField = revealeCellWithAdjecentCells({minesField: newMinesField, cellKey, size});
    })
  }
  
  return newMinesField;
};

