import { isGameOver } from '../selectors/game-selectors';
import { markCell } from '../services/cell-marker';
import { revealeCell, revealeCellFirstOrderNeighbours } from '../services/cells-revealer';
import { getNewGame } from '../services/game-generator';
import { stopTimer } from '../services/timer';
import { Action, GameData, MARK_CELL, NEW_GAME, REVEALE_CELL, REVEALE_NEIGHBOURS, State, TICK } from '../types';


const tick = (game: GameData) => {
  const elapsed = game.elapsed < 999 ? game.elapsed + 1 : game.elapsed;
  return { ...game, elapsed };
};

const revealeCellInMinesField = (game: GameData, cellKey: number) => {
  const { minesField, size, mines } = game;
  const newMinesField = revealeCell({minesField, cellKey, size, mines});
  isGameOver(newMinesField) && stopTimer();
  return { ...game, minesField: newMinesField, };
};

const revealeCellNeighbours = (game: GameData, cellKey: number) => {
  const { minesField, size } = game;
  const newMinesField = revealeCellFirstOrderNeighbours({minesField, cellKey, size});
  return { ...game, minesField: newMinesField, };
};

const markCellInMinesField = (game: GameData, key: number) => {
  const { minesField } = game;
  return { ...game, minesField: markCell(minesField, key) };
};

const newGame = (size: number, mines: number): GameData => {
  stopTimer();
  return {
    size,
    elapsed: 0,
    minesField: getNewGame(size),
    mines
  };
};

export const  reducer = (state: State, action: Action): State =>  {
  const { game } = state;
  switch (action.type) {
    case REVEALE_CELL:
      return { ...state, game: revealeCellInMinesField(game, action.key) };
    case REVEALE_NEIGHBOURS:
        return { ...state, game:  revealeCellNeighbours(game, action.key)};
    case MARK_CELL:
      return { ...state, game: markCellInMinesField(game, action.key) };
    case NEW_GAME:
      return { ...state, game: newGame(action.size, action.mines) };
    case TICK:
      return { ...state, game: tick(game) };
    default:
      throw new Error();
  }
}
