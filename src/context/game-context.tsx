import { createContext, useContext } from 'react';
import { Action, State } from '../types';

export const GameStateContext = createContext({} as State);
export const GameDispatchContext = createContext(({}) as React.Dispatch<Action>);

export const useGame = () => useContext(GameStateContext).game;
export const useMinesField= () => useGame().minesField;
export const useCellData= (cellKey: number) => useMinesField()[cellKey];

export const useDispatch= () => useContext(GameDispatchContext);
