import { CellData } from '../types';

const getGameCellsCountByFilter = (
  minesField: CellData[],
  test: (cell: CellData) => boolean | undefined,
) => minesField.filter(cell => test(cell)).length;

export const getNumberOfMines = (minesField: CellData[]): number =>
  getGameCellsCountByFilter(minesField, (cell) => cell.isMine);

export const getNumberOfRevealed = (minesField: CellData[]): number =>
  getGameCellsCountByFilter(minesField, (cell) => cell.isRevealed);

export const getNumberOfFlagged = (minesField: CellData[]): number =>
  getGameCellsCountByFilter(minesField, (cell) => cell.isMarkedAsMine);

const areAllNonMineCellsRevealed = (minesField: CellData[]) =>
minesField.length -
  getNumberOfRevealed(minesField) -
  getNumberOfMines(minesField) === 0;

export const isGameWon= (minesField: CellData[]) => areAllNonMineCellsRevealed(minesField) ;

export const isGameLost = (minesField: CellData[]) =>
  getGameCellsCountByFilter(
    minesField,
    (cell) => cell.isMine && cell.isRevealed,
  ) !== 0;

export const isGameOver= (minesField: CellData[]) => isGameWon(minesField) || isGameLost(minesField);
export const isGameStarted = (minesField: CellData[]) =>
  getNumberOfRevealed(minesField) !== 0;

export const isCellRevealed = (minesField: CellData[], cellKey: number) => minesField[cellKey].isRevealed;