import { Action, REVEALE_NEIGHBOURS } from "../types";

export const revealeNeighboursAction = (key: number) : Action => ({
  type: REVEALE_NEIGHBOURS,
  key
});
