export { revealeCellAction } from './reveale-cell';
export { revealeNeighboursAction } from './reveale-neighbours';
export { markCellAction } from './mark-cell';
export { newGameAction } from './new-game';
export { tickAction } from './tick-action';