import { Action, MARK_CELL } from '../types';

export const markCellAction = (key: number) : Action => ({
  type: MARK_CELL,
  key
});
