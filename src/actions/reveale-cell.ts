import { Action, REVEALE_CELL } from "../types";

export const revealeCellAction = (key: number) : Action => ({
  type: REVEALE_CELL,
  key
});
