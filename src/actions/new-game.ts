import { Action, NEW_GAME } from '../types';

export const newGameAction = (size: number, mines: number): Action => ({
  type: NEW_GAME,
  size,
  mines
});
