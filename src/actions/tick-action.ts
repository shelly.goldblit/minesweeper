import { Action, TICK } from "../types";

export const tickAction = () : Action => ({
  type: TICK,
});
