export interface CellData {
  isMine?: boolean;
  neighbours: number;
  isRevealed?: boolean;
  isMarkedAsMine?: boolean;
  isMarkedAsSuspect?: boolean;
  didExplode?: boolean;
}

export interface GameData {
  minesField: CellData[];
  size: number;
  elapsed: number;
  mines: number;
}


export type State = {
  game: GameData;
};

export const NEW_GAME = 'NEW_GAME';
export const REVEALE_CELL = 'REVEALE_CELL';
export const REVEALE_NEIGHBOURS = 'REVEALE_NEIGHBOURS';
export const MARK_CELL = 'MARK_CELL';
export const TICK = 'TICK';

export type Action =
  | { type: 'NEW_GAME' ; size: number; mines: number; }
  | { type: 'REVEALE_CELL'; key: number; }
  | { type: 'REVEALE_NEIGHBOURS'; key: number; }
  | { type: 'MARK_CELL'; key: number }
  | { type: 'TICK' }
