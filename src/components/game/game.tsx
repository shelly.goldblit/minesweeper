import { MinesField } from "../mines-field/mines-field";
import { GameStatus } from "../game-status/game-status";
import './game.scss';

export interface GameProps {
  size: number;
}

export const Game= ({size} : GameProps) =>{
  return (
      <div className='board'>
        <GameStatus/>
        <MinesField size={size}/>
      </div>
  );
}