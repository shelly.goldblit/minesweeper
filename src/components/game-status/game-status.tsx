import './game-status.scss';

import { useDispatch, useGame } from "../../context/game-context";
import { getNumberOfFlagged, isGameOver, isGameWon } from "../../selectors/game-selectors";
import { newGameAction } from '../../actions/new-game';
import { useCallback } from 'react';


export interface GameStatusProps {
}
const Counter = (props : {value : number}) => <span className='counter'>{props.value.toString().padStart(3, '0')}</span>

export const GameStatus= (props : GameStatusProps) =>{
  const {elapsed, mines, size, minesField}= useGame();
  const dispatch = useDispatch();
  const gameOverStatus = isGameWon(minesField)? '😎' : '😖';
  const gameStatus = isGameOver(minesField)? gameOverStatus : '🙂';

  const onClick = useCallback(() => dispatch(newGameAction(size, mines)),[dispatch, mines, size]);
  
  return (
    <>
    <div className='status'>
      <Counter value={elapsed}/>
      <button className='status-button' onClick={onClick}>{gameStatus}</button>
      <Counter value={mines-getNumberOfFlagged(minesField)}/>
    </div>
    </>
  );
}