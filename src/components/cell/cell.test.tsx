import '@testing-library/jest-dom/extend-expect';
import { CellDriver } from './cell.driver';
import { Chance } from 'chance';

describe('Cell tests', () => {
  let driver: CellDriver;
  const chance = new Chance();

  beforeEach(() => {
    driver = new CellDriver();
  });

  describe('snapshots', () => {
      test.each`
      neighbours 
      ${1}       
      ${2}       
      ${3}       
      ${4}       
      ${5}       
      ${6}      
      ${7}       
      ${8}      
    `(
      'should render cell when neighbours = $neighbours for non mine cell',
      ({ neighbours }) => {
        driver.given 
        .isRevealed(false)
        .given.neighbours(neighbours)
        .when.render();

        expect(driver.get.cellComponent()).toMatchSnapshot();
      },
    );

    test('should render an unmarked unrevealed cell', () => {
      driver.given 
        .isRevealed(false)
        .given.neighbours(4)
        .when.render();
  
      expect(driver.get.cellComponent()).toMatchSnapshot();
    });
  
    test('should render an unrevealed cell marked with question mark', () => {
      driver.given.isMarkedAsSuspect(true).given.isRevealed(false).when.render();
  
      expect(driver.get.cellComponent()).toMatchSnapshot();
    });
    test('should render an unrevealed cell marked as mine', () => {
      driver.given.isMarkedAsMine(true).given.isRevealed(false).when.render();
  
      expect(driver.get.cellComponent()).toMatchSnapshot();
    });
  });

  describe('cell text', () => {
    let neighbours : number;
    beforeEach(() => {
      neighbours = chance.integer({ min: 0, max: 8 });
    });

    test('should render no text when an unmarked unrevealed cell', () => {
      driver.given
        .isRevealed(false)
        .given.isMine(chance.bool())
        .given.neighbours(chance.integer({ min: 0, max: 8 }))
        .when.render();
  
      expect(driver.get.cellTextContent()).toBe('');
    });
  
    test('should render a revealed mine cell', () => {
      driver.given
        .isRevealed(true)
        .given.isMine(true)
        .given.neighbours(neighbours)
        .when.render();
  
      expect(driver.get.cellTextContent()).toBe('💣');
    });
  
    test('should render a revealed neighbours cell', () => {
      driver.given
        .isRevealed(true)
        .given.isMine(false)
        .given.neighbours(neighbours)
        .when.render();
      expect(driver.get.cellTextContent()).toBe(neighbours.toString());
    });

    test('should render a non revealed marked as mine cell', () => {
      driver.given
        .isRevealed(false)
        .given.isMine(chance.bool())
        .given.neighbours(neighbours)
        .given.isMarkedAsMine(true)
        .when.render();
      expect(driver.get.cellTextContent()).toBe('🚩');
    });

    test('should render a non revealed marked as suspect cell', () => {
      driver.given
        .isRevealed(false)
        .given.isMine(chance.bool())
        .given.neighbours(neighbours)
        .given.isMarkedAsSuspect(true)
        .when.render();
      expect(driver.get.cellTextContent()).toBe('?');
    });

    test('should render a wrongly marked as mine cell when shouldIndicateWrongMark', () => {
      driver.given
        .isRevealed(false)
        .given.isMine(false)
        .given.neighbours(neighbours)
        .given.isMarkedAsMine()
        .given.shouldIndicateWrongMark()
        .when.render();
      expect(driver.get.cellTextContent()).toBe('❌');
    });

    test('should render a flag for wrongly marked as mine cell when shouldIndicateWrongMark is false', () => {
      driver.given
        .isRevealed(false)
        .given.isMine()
        .given.neighbours(neighbours)
        .given.isMarkedAsMine()
        .given.shouldIndicateWrongMark(false)
        .when.render();
      expect(driver.get.cellTextContent()).toBe('🚩');
    });
  });  

  
  describe('callbacks', () => {
    test('should call the callback handler when clicked', () => {
      const onClick = jest.fn();
      driver.given.onClick(onClick).when.render().when.clickingCell();
  
      expect(onClick).toHaveBeenCalledTimes(1);
    });
  
    test('should call the callback handler when right clicked', () => {
      const onClick = jest.fn();
      driver.given.onRightClick(onClick).when.render().when.rightClickingCell();
      expect(onClick).toHaveBeenCalledTimes(1);
    });

    test('should call the callback handler when double clicked', () => {
      const onDoubleClick = jest.fn();
      driver.given.onDoubleClick(onDoubleClick).when.render().when.doubleClickingCell();
  
      expect(onDoubleClick).toHaveBeenCalledTimes(1);
    });
  });

  
  
});
