
import './cell.scss';
import classNames from 'classnames';
import { useCallback } from 'react';

export interface CellComponentProps {
  isMine?: boolean;
  neighbours: number;
  isRevealed?: boolean;
  isMarkedAsMine?: boolean;
  isMarkedAsSuspect?: boolean;
  shouldIndicateWrongMark? : boolean,
  didExpload?: boolean,
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onDoubleClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onRightClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

export const CellComponent = (props: CellComponentProps) => {
  const {
    onClick,
    onDoubleClick,
    onRightClick,
    isMine,
    neighbours,
    isRevealed,
    isMarkedAsMine,
    isMarkedAsSuspect,
    shouldIndicateWrongMark,
    didExpload,
  } = props;

  const cellClass = classNames(
    'cell',
    { 'revealed': isRevealed },
    { 'unrevealed': !isRevealed },
    { [`neighbours-${neighbours}`]: !isMine },
    { 'KABOOM': didExpload },
  );

  const getCellContent = (useCallback(() => {
    const bomb = '\uD83D\uDCA3';
    const flag = '\uD83D\uDEA9';
    const cross = '\u274C';

    if (shouldIndicateWrongMark && isMarkedAsMine && !isMine)
    {
      return cross;
    }
    if (isRevealed) {
      return isMine? bomb : neighbours;
    }
    if (isMarkedAsMine) {
      return flag;
    } 
    if (isMarkedAsSuspect) {
      return '?'
    }
  }, [isMarkedAsMine, isMarkedAsSuspect, shouldIndicateWrongMark, isRevealed, isMine, neighbours]));
 
  return (
    <button
      className={cellClass}
      onClick={onClick}
      onDoubleClick={onDoubleClick}
      data-hook={`cell`}
      onContextMenu={onRightClick}
    >{getCellContent()}</button>
  );
};