// import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import { CellComponent, CellComponentProps } from './cell.component';

export class CellDriver {
  private props: CellComponentProps = {
    onClick: () => {},
    onRightClick: () => {},
    onDoubleClick: () => {},
    neighbours: 0,
  };

  private component: HTMLElement | undefined;
  private container: HTMLElement | undefined;

  given = {
    neighbours: (neighbours: number): CellDriver => {
      this.props.neighbours = neighbours;
      return this;
    },
    isMine: (isMine: boolean = true): CellDriver => {
      this.props.isMine = isMine;
      return this;
    },
    isMarkedAsSuspect: (isMarkedAsSuspect: boolean = true): CellDriver => {
      this.props.isMarkedAsSuspect = isMarkedAsSuspect;
      return this;
    },
    isMarkedAsMine: (isMarkedAsMine: boolean = true): CellDriver => {
      this.props.isMarkedAsMine = isMarkedAsMine;
      return this;
    },
    isRevealed: (isRevealed: boolean = true): CellDriver => {
      this.props.isRevealed = isRevealed;
      return this;
    },
    shouldIndicateWrongMark: (shouldIndicateWrongMark: boolean = true): CellDriver => {
      this.props.shouldIndicateWrongMark = shouldIndicateWrongMark;
      return this;
    },
    
    onClick: (onClick: () => void) => {
      this.props.onClick = onClick;
      return this;
    },
    onDoubleClick: (onDoubleClick: () => void) => {
      this.props.onDoubleClick = onDoubleClick;
      return this;
    },
    onRightClick: (onRightClick: () => void) => {
      this.props.onRightClick = onRightClick;
      return this;
    },
  };
  get = {
    cellComponent: () => {
      return this.component;
    },
    cellTextContent: () => {
      return this.component?.textContent;
    }
  };
  when = {
    render: (): CellDriver => {
      const { getByTestId, container } = render(<CellComponent {...this.props} />);
      this.container = container;
      this.component = getByTestId('cell');
      return this;
    },
    clickingCell: (): CellDriver => {
      fireEvent.click(this.component!);
      return this;
    },
    doubleClickingCell: (): CellDriver => {
      fireEvent.doubleClick(this.component!);
      return this;
    },
    rightClickingCell: (): CellDriver => {
      fireEvent.contextMenu(this.component!);
      return this;
    },
  };
};
