

import { useCallback } from 'react';
import { CellComponent } from './cell.component';
import { useCellData, useDispatch, useMinesField } from '../../context/game-context';
import { markCellAction, revealeCellAction, revealeNeighboursAction, tickAction } from '../../actions/actions';
import { isGameStarted } from '../../selectors/game-selectors';
import { startTimer } from '../../services/timer';

export interface CellProps {
  cellKey: number;
}

export const Cell = (props: CellProps) => {
  const { cellKey } = props;
  const cellData = useCellData(cellKey);
  const dispatch = useDispatch();
  const gameStarted = isGameStarted(useMinesField());
  const { isRevealed, isMarkedAsMine, isMarkedAsSuspect, isMine, neighbours, didExplode} = cellData;
  const onClick=useCallback((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    !gameStarted && startTimer(() => dispatch(tickAction()));
    dispatch(revealeCellAction(cellKey))
  }, [cellKey, dispatch, gameStarted]);

  const onRightClick=useCallback((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    dispatch(markCellAction(cellKey))
  }, [cellKey, dispatch]);

  const onDoubleClick=useCallback((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    dispatch(revealeNeighboursAction(cellKey))
  }, [cellKey, dispatch]);
  
  return (
   <CellComponent 
    isRevealed={isRevealed} 
    isMarkedAsMine={isMarkedAsMine} 
    isMarkedAsSuspect={isMarkedAsSuspect}
    isMine={isMine}
    neighbours={neighbours}
    didExpload={didExplode}
    onClick={onClick}
    onDoubleClick={onDoubleClick}
    onRightClick={onRightClick}
    />
  );
};