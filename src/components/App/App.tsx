import React from 'react';
import './App.css';
import { MineSweeper } from '../mine-sweeper/mine-sweeper';

interface AppProps {}

class App extends React.Component<AppProps> {  
  render () {
    return (
      <div className="App">
        <MineSweeper size={5} mines={3}/>
      </div>
    );
  }
}

export default App;
