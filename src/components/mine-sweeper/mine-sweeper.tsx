import  { useReducer } from "react";
import { reducer } from "../../context/reducer";
import { GameDispatchContext, GameStateContext } from '../../context/game-context';
import { getNewGame } from "../../services/game-generator";
import { Game } from "../game/game";
export interface MineSweeperProps {
  size: number;
  mines: number;
}

export const MineSweeper= ({size, mines} : MineSweeperProps) =>{
  const [state, dispatch] = useReducer(reducer, { game: {
    minesField: getNewGame(size),
    size,
    elapsed: 0,
    mines,
  }});

  return (
    <GameDispatchContext.Provider value={dispatch}>
      <GameStateContext.Provider value={state}>
          <Game size={size}/>
      </GameStateContext.Provider>
    </GameDispatchContext.Provider>
  );
}